package com.frameworkium.integration.yorktesters.tests;

import com.frameworkium.core.ui.tests.BaseUITest;
import com.frameworkium.integration.yorktesters.pages.SkyNewsPage;
import org.testng.annotations.Test;

import static com.frameworkium.integration.yorktesters.pages.Helper.scrollToHeader;
import static com.frameworkium.integration.yorktesters.pages.Helper.scrollToView;
import static com.frameworkium.integration.yorktesters.pages.Helper.waitForOnScreenAnimation;
import static com.google.common.truth.Truth.assertThat;

@Test
public class SkyNewsTest extends BaseUITest {

    //Open Sky News UK Page and go to the click load more button, then up and down a bit
    public void openSkyNewsPage() {
        SkyNewsPage skyNews = SkyNewsPage.open();

        skyNews.clickNavBarText("UK");

        assertThat(skyNews
                .isTopTileHeroArticle())
                .isTrue();

        skyNews.clickLoadMoreButton();

        waitForOnScreenAnimation(5000);

        scrollToHeader();

        waitForOnScreenAnimation(5000);

        scrollToView("Videos");

        waitForOnScreenAnimation(5000);
    }

    public void navigateSkyNewsPages() {
        SkyNewsPage skyNewsPage = SkyNewsPage.open();


    }
}
