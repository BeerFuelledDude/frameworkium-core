package com.frameworkium.integration.yorktesters.tests;

import com.frameworkium.core.ui.tests.BaseUITest;
import com.frameworkium.integration.yorktesters.pages.DataProviderClass;
import com.frameworkium.integration.yorktesters.pages.SkyNewsPage;
import com.google.common.truth.Truth;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class DataProviderTest extends BaseUITest {

    @Test(dataProvider = "pages", dataProviderClass = DataProviderClass.class)
    @Parameters({"page"})
    public void skyPagesTest(@Optional String page) {
        SkyNewsPage skyNewsPage = SkyNewsPage.open();

        String selectedText = skyNewsPage.clickAcceptPrivacyOption()
                .clickNavBarText(page)
                .getSelectedMenuItemsText();

        Truth.assertWithMessage("Selected Menu Item expected " + page)
                .that(selectedText)
                .isEqualTo(page);
    }
}

