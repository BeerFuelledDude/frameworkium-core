package com.frameworkium.integration.yorktesters.tests.api;

import com.frameworkium.core.api.tests.BaseAPITest;
import com.frameworkium.integration.yorktesters.pages.api.common.CommonSteps;
import com.frameworkium.integration.yorktesters.pages.api.request.PostRequestBody;
import com.frameworkium.integration.yorktesters.pages.api.request.query.Query;
import com.frameworkium.integration.yorktesters.pages.api.response.Root;
import com.frameworkium.integration.yorktesters.pages.api.response.RootError;
import io.restassured.response.Response;
import org.junit.Test;

import static com.google.common.truth.Truth.assertWithMessage;

public class StatusCodesTest extends BaseAPITest {

    private CommonSteps commonSteps = new CommonSteps();

    @Test
    public void sizeOnly() {
        Response response = commonSteps
                .submitPostRequestToAnEndpointWithBody("http://localhost:9200/_all/alert/_search",
                        commonSteps.emptyMatchAll());

        response.prettyPrint();

        String sizeReturned = response.getBody().as(Root.class).getHits().getTotal();
        int size = Integer.valueOf(sizeReturned);

        assertWithMessage("0 returned for total").that(size).isGreaterThan(0);
    }

    @Test
    public void isStatusCode200() {
        Response response = commonSteps.submitPostRequestToAnEndpoint("http://localhost:9200/_all/alert/_search");

        int statusCode = response.getStatusCode();

        assertWithMessage("Status code does not equal 200").that(statusCode)
                .isEqualTo(200);
    }

    @Test
    public void isStatusCode400WithReasonForNullQuery() {
        PostRequestBody incompleteQuery = new PostRequestBody();
        Response response = commonSteps.submitPostRequestToAnEndpointWithBody("http://localhost:9200/_all/alert/_search", incompleteQuery);

        int statusCode = response.getStatusCode();
        String errorReason = response.getBody().as(RootError.class).getError().getReason();

        assertWithMessage("Status code does not equal 400 for a bad request").that(statusCode)
                .isEqualTo(400);

        assertWithMessage("Error reason not correct").that(errorReason)
                .contains("Unknown key for a VALUE_NULL in [query]");
    }

    @Test
    public void isStatusCode400WithReasonForNullBool() {
        PostRequestBody incompleteBool = new PostRequestBody();
        Query query = new Query();
        incompleteBool.setQuery(query);
        Response response = commonSteps.submitPostRequestToAnEndpointWithBody("http://localhost:9200/_all/alert/_search", incompleteBool);

        int statusCode = response.getStatusCode();
        String errorReason = response.getBody().as(RootError.class).getError().getReason();

        assertWithMessage("Status code does not equal 400 for a bad request").that(statusCode)
                .isEqualTo(400);

        assertWithMessage("Error reason not correct").that(errorReason)
                .contains("[bool] query malformed, no start_object after query name");
    }
}
