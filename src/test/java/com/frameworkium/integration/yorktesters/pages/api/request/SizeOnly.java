package com.frameworkium.integration.yorktesters.pages.api.request;

public class SizeOnly {

    private int size;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
