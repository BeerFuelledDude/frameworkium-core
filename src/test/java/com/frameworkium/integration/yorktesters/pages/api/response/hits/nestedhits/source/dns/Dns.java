package com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns.request.Request;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns.response.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Dns {

    private Request request;
    private Response response;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
