package com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns.response.answers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Answers {

    private String name;
    private int type;
    private int timeToLive;
    private String rData;
    private String rawRData;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTimeToLive() {
        return timeToLive;
    }

    public void setTimeToLive(int timeToLive) {
        this.timeToLive = timeToLive;
    }

    public String getrData() {
        return rData;
    }

    public void setrData(String rData) {
        this.rData = rData;
    }

    public String getRawRData() {
        return rawRData;
    }

    public void setRawRData(String rawRData) {
        this.rawRData = rawRData;
    }
}
