package com.frameworkium.integration.yorktesters.pages;

import com.frameworkium.core.ui.js.JavascriptWait;
import com.frameworkium.core.ui.tests.BaseUITest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.frameworkium.core.ui.tests.BaseUITest.getWebDriver;


public class Helper {

    private static WebDriver driver = BaseUITest.getWebDriver();
    private static WebDriverWait wait = new WebDriverWait(driver, 5);
    private static JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
    private static JavascriptWait jsWait = new JavascriptWait(javascriptExecutor, wait);

    private static String loadMoreButton = "sdc-site-load-more__button";

    public static void waitForOnScreenAnimation(int durationInMilliseconds) {
        try {
            Thread.sleep(durationInMilliseconds);
        } catch (InterruptedException e) {
            //Ignore
        }
    }

    public static void scrollToHeader() {
        WebElement element = driver.findElement(By.className("sdc-site-component-header--h1"));
        scrollToViewJS(element);
    }

    public static void scrollToLoadMoreButton() {
        WebElement element = driver.findElement(By.className(loadMoreButton));
        scrollToViewJS(element);
    }

    public static void moveToLoadMoreButton() {
        Actions actions = new Actions(driver);
        WebElement element = driver.findElement(By.className(loadMoreButton));
        actions.moveToElement(element);
        actions.perform();
    }

    public static void scrollToView(String name) {
        WebElement element = getBodyHeaderElement(name);
        scrollToViewJS(element);
    }

    private static void scrollToViewJS(WebElement element) {
        ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
        waitForOnScreenAnimation(2000);
    }

    private static WebElement getBodyHeaderElement(String name) {
        return driver.findElements(By.cssSelector("[data-role='short-text-target']"))
                .stream()
                .filter(e -> e.getText()
                        .equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }
}
