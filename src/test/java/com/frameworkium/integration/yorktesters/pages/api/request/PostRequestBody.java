package com.frameworkium.integration.yorktesters.pages.api.request;

import com.frameworkium.integration.yorktesters.pages.api.request.query.Query;

import java.io.Serializable;

public class PostRequestBody implements Serializable {

    private Query query;
    private int from;
    private int size;

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }
}
