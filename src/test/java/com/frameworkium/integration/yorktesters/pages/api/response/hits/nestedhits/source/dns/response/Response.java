package com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns.request.questions.Questions;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns.response.answers.Answers;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

    private String transactionId;
    private String opCode;
    private String responseCode;
    private List<Questions> questions;
    private List<Answers> answers;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOpCode() {
        return opCode;
    }

    public void setOpCode(String opCode) {
        this.opCode = opCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public List<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Questions> questions) {
        this.questions = questions;
    }

    public List<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }
}
