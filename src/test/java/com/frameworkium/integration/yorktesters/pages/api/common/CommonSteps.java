package com.frameworkium.integration.yorktesters.pages.api.common;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.frameworkium.integration.yorktesters.pages.api.ReusableSpecifications;
import com.frameworkium.integration.yorktesters.pages.api.request.PostRequestBody;
import com.frameworkium.integration.yorktesters.pages.api.request.query.Query;
import com.frameworkium.integration.yorktesters.pages.api.request.query.bool.Bool;
import com.frameworkium.integration.yorktesters.pages.api.request.query.bool.must.Must;
import com.frameworkium.integration.yorktesters.pages.api.request.query.bool.must.Term.Term;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER;
import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS;
import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES;

public class CommonSteps {

    private Term term = new Term();
    private Must must = new Must();
    private List<Must> mustList = new ArrayList<>();
    private static Bool bool = new Bool();
    private static Query query = new Query();
    private static PostRequestBody postRequestBody = new PostRequestBody();
    private static ObjectMapper objectMapper = new ObjectMapper();

    @Step("Submit a GET request to '{0}' endpoint")
    public Response submitGetRequestToAnEndpoint(String endpoint) {
        //send GET request
        return RestAssured
                .given()
                .when()
                .get(endpoint);
    }

    @Step("Submit a POST request to '{0}' endpoint with body")
    public Response submitPostRequestToAnEndpointWithBody(String endpoint, Object body) {
        String bodyString;
        objectMapper.configure(ALLOW_UNQUOTED_CONTROL_CHARS, true);
        objectMapper.configure(ALLOW_UNQUOTED_FIELD_NAMES, true);
        objectMapper.configure(ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
        try {
            bodyString = objectMapper.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
        //send POST request
        return RestAssured
                .given()
                .spec(ReusableSpecifications.getGenericRequestSpecification())
                .when()
                .body(bodyString)
                .post(endpoint);
    }

    @Step("Submit a POST request to '{0}' endpoint without body")
    public Response submitPostRequestToAnEndpoint(String endpoint) {
        //send POST request
        return RestAssured
                .given()
                .spec(ReusableSpecifications.getGenericRequestSpecification())
                .when()
                .post(endpoint);
    }

    @Step("test")
    public PostRequestBody emptyMatchAll() {
        must.setMatch_all(new HashMap<>());
        mustList.add(must);
        bool.setMust(mustList);
        query.setBool(bool);
        postRequestBody.setQuery(query);
        postRequestBody.setSize(50);

        return postRequestBody;

    }

    @Step("Setup Post Body with EventID {0}")
    public PostRequestBody setupPostBodyWithEventId(String eventId) {
        term.setEventId(eventId);
        must.setTerm(term);
        mustList.add(must);
        bool.setMust(mustList);
        query.setBool(bool);
        postRequestBody.setQuery(query);
        postRequestBody.setSize(50);

        return postRequestBody;
    }
}
