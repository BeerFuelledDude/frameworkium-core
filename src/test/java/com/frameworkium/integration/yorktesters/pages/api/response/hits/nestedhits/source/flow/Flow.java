package com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.flow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Flow {

    private String ipAddress1;
    private String ipAddress2;
    private int port1;
    private int port2;
    private int ipProtocol;

    public String getIpAddress1() {
        return ipAddress1;
    }

    public void setIpAddress1(String ipAddress1) {
        this.ipAddress1 = ipAddress1;
    }

    public String getIpAddress2() {
        return ipAddress2;
    }

    public void setIpAddress2(String ipAddress2) {
        this.ipAddress2 = ipAddress2;
    }

    public int getPort1() {
        return port1;
    }

    public void setPort1(int port1) {
        this.port1 = port1;
    }

    public int getPort2() {
        return port2;
    }

    public void setPort2(int port2) {
        this.port2 = port2;
    }

    public int getIpProtocol() {
        return ipProtocol;
    }

    public void setIpProtocol(int ipProtocol) {
        this.ipProtocol = ipProtocol;
    }
}
