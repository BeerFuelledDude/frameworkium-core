package com.frameworkium.integration.yorktesters.pages.api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.lessThan;

public class ReusableSpecifications {

    public static RequestSpecBuilder build;
    public static RequestSpecification rspec;

    public static ResponseSpecBuilder respspec;
    public static ResponseSpecification responseSpecification;

    // Used to set up the commonly used request header details
    public static RequestSpecification getGenericRequestSpecification() {

        build = new RequestSpecBuilder();
        build.setContentType(ContentType.JSON);

        rspec = build.build();

        return rspec;
    }

    public static ResponseSpecification getGenericResponseSpecification() {

        respspec = new ResponseSpecBuilder();
        respspec.expectHeader("Content-Type", "application/json; charset=utf-8");
        respspec.expectResponseTime(lessThan(5L), TimeUnit.SECONDS);
        responseSpecification = respspec.build();

        return responseSpecification;
    }
}
