package com.frameworkium.integration.yorktesters.pages;

import org.testng.annotations.DataProvider;


public class DataProviderClass {

    @DataProvider(name = "pages", parallel = false)
    public static Object[][] skyPages() {
        return new Object[][]
                {
                        {"UK"},
                        {"World"},
                        {"Politics"},
                        {"US"},
                        {"Ocean Rescue"},
                        {"Science & Tech"},
                        {"Business"},
                        {"Ents & Arts"},
                        {"Offbeat"}
                };
    }
}
