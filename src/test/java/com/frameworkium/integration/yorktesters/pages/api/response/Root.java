package com.frameworkium.integration.yorktesters.pages.api.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.Hits;
import com.frameworkium.integration.yorktesters.pages.api.response.shards.Shards;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Root {

    private String took;
    private String timed_out;
    private Hits hits;
    private Shards _shards;

    public Shards get_shards() {
        return _shards;
    }

    public void set_shards(Shards _shards) {
        this._shards = _shards;
    }

    public Hits getHits() {
        return hits;
    }

    public void setHits(Hits hits) {
        this.hits = hits;
    }

    public String getTook() {
        return took;
    }

    public void setTook(String took) {
        this.took = took;
    }

    public String getTimed_out() {
        return timed_out;
    }

    public void setTimed_out(String timed_out) {
        this.timed_out = timed_out;
    }
}
