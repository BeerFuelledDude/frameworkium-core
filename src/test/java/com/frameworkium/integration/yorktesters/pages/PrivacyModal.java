package com.frameworkium.integration.yorktesters.pages;

import com.frameworkium.core.ui.pages.PageFactory;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

public class PrivacyModal extends HtmlElement {

    @FindBy(css = "button[class^='sp_choice']")
    private Button acceptButton;

    @Step
    public SkyNewsPage clickAcceptButton() {
        acceptButton.click();
        return PageFactory.newInstance(SkyNewsPage.class);
    }
}
