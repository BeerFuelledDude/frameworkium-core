package com.frameworkium.integration.yorktesters.pages;

import com.frameworkium.core.ui.annotations.Visible;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

import static com.frameworkium.integration.yorktesters.pages.Helper.waitForOnScreenAnimation;

public class NavigationBar extends HtmlElement {

    @Visible
    @FindBy(className = "sdc-site-header__logo")
    private WebElement skyNewsLogo;

    @FindBy(css = "li [aria-current]")
    private WebElement selectedMenuItem;

    @FindBy(className = "sdc-site-header__action-button")
    private WebElement watchLiveButton;

    @FindBy(className = "sdc-site-header__menu")
    private WebElement navMenu;

    @FindBy(className = "sdc-site-header__menu-item-link")
    private List<WebElement> navButtons;

    @FindBy(css = "span[data-role='short-text-target']")
    private WebElement title;

    @Step("Click {0} in navigation bar link")
    NavigationBar clickNavigationBarLink(String navLink) {
        navMenu
                .findElements(By.className("sdc-site-header__menu-item-link"))
                .stream()
                .filter(e -> e.getText()
                        .equalsIgnoreCase(navLink))
                .findFirst()
                .orElseThrow(NotFoundException::new)
                .click();

        waitForOnScreenAnimation(500);
        return this;
    }

    @Step("Get selected menu item")
    String getSelectedMenuItemText() {
        return selectedMenuItem.getText();
    }
}
