package com.frameworkium.integration.yorktesters.pages.api.request.query;

import com.frameworkium.integration.yorktesters.pages.api.request.query.bool.Bool;

import java.io.Serializable;

public class Query implements Serializable {

    private Bool bool;

    public Bool getBool() {
        return bool;
    }

    public void setBool(Bool bool) {
        this.bool = bool;
    }
}
