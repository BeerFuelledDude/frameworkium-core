package com.frameworkium.integration.yorktesters.pages.api.response;

import com.frameworkium.integration.yorktesters.pages.api.response.error.Error;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RootError {

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    private Error error;
}
