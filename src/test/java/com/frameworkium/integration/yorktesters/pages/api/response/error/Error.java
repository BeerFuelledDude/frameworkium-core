package com.frameworkium.integration.yorktesters.pages.api.response.error;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.frameworkium.integration.yorktesters.pages.api.response.error.rootcause.RootCause;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Error {

    private List<RootCause> root_cause;
    private String type;
    private String reason;

    public List<RootCause> getRoot_cause() {
        return root_cause;
    }

    public void setRoot_cause(List<RootCause> root_cause) {
        this.root_cause = root_cause;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
