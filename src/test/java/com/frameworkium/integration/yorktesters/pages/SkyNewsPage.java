package com.frameworkium.integration.yorktesters.pages;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static com.frameworkium.integration.yorktesters.pages.Helper.scrollToLoadMoreButton;
import static com.frameworkium.integration.yorktesters.pages.Helper.waitForOnScreenAnimation;

public class SkyNewsPage extends BasePage<SkyNewsPage> {

    public static SkyNewsPage open() {
        return PageFactory.newInstance(SkyNewsPage.class,
                "https://news.sky.com/");
    }

    @FindBy(css = "div [id^='sp_message']")
    private PrivacyModal privacyModal;

    @FindBy(className = "sdc-site-header__inner")
    private NavigationBar navigationBar;

    @FindBy(css = "span [data-role='short-text-target']")
    private WebElement pageTitle;

    @FindBy(css = "div [class*='sdc-site-tiles__item sdc-site-tile sdc-site-tile--has-link']")
    private List<WebElement> newsTiles;

    @FindBy(className = "sdc-site-load-more__button")
    private WebElement loadMoreButton;

    @Step("Click Text in Navigation bar")
    public SkyNewsPage clickNavBarText(String navLink) {
        navigationBar
                .clickNavigationBarLink(navLink);
        return PageFactory.newInstance(SkyNewsPage.class);
    }

    @Step("Get First News Article")
    public boolean isTopTileHeroArticle() {
        return newsTiles
                .get(0)
                .getAttribute("data-type")
                .contains("hero");
    }

    @Step("Click Load More Button")
    public SkyNewsPage clickLoadMoreButton() {
        scrollToLoadMoreButton();
        loadMoreButton.click();
        return this;
    }

    @Step("Click Accept privacy option")
    public SkyNewsPage clickAcceptPrivacyOption() {
        if (privacyModal.isDisplayed()) {
            privacyModal.clickAcceptButton();
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div [id^='sp_message']")));
            waitForOnScreenAnimation(500);
        }
        return this;
    }

    @Step("Get selected menu item's text")
    public String getSelectedMenuItemsText() {
        return navigationBar.getSelectedMenuItemText();
    }
}
