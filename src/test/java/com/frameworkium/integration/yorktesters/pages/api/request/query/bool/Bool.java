package com.frameworkium.integration.yorktesters.pages.api.request.query.bool;

import com.frameworkium.integration.yorktesters.pages.api.request.query.bool.must.Must;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Bool implements Serializable {

    private List<Must> must = new ArrayList<>();

    public List<Must> getMust() {
        return must;
    }

    public void setMust(List<Must> must) {
        this.must = must;
    }
}