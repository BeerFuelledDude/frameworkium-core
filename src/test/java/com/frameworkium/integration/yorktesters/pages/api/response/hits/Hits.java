package com.frameworkium.integration.yorktesters.pages.api.response.hits;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.NestedHits;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Hits {
    private String total;

    private String max_score;
    private List<NestedHits> hits = new ArrayList<>();

    public List<NestedHits> getHits() {
        return hits;
    }

    public void setHits(List<NestedHits> hits) {
        this.hits = hits;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getMax_score() {
        return max_score;
    }

    public void setMax_score(String max_score) {
        this.max_score = max_score;
    }
}
