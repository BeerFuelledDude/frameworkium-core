package com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.alerts.Alerts;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns.Dns;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.flow.Flow;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Source {

    private String eventId;
    private Long startTime;
    private Long endTime;
    private Flow flow;
    private Alerts alerts;
    private Dns dns;

    public Alerts getAlerts() {
        return alerts;
    }

    public void setAlerts(Alerts alerts) {
        this.alerts = alerts;
    }

    public Dns getDns() {
        return dns;
    }

    public void setDns(Dns dns) {
        this.dns = dns;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Flow getFlow() {
        return flow;
    }

    public void setFlow(Flow flow) {
        this.flow = flow;
    }
}
