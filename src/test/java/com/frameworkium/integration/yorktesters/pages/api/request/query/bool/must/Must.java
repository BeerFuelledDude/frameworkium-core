package com.frameworkium.integration.yorktesters.pages.api.request.query.bool.must;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.frameworkium.integration.yorktesters.pages.api.request.query.bool.must.Term.Term;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Must implements Serializable {

    private Term term;
    private Object match_all;

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public Object getMatch_all() {
        return match_all;
    }

    public void setMatch_all(Object match_all) {
        this.match_all = match_all;
    }
}