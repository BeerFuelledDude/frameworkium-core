package com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.frameworkium.integration.yorktesters.pages.api.response.hits.nestedhits.source.dns.request.questions.Questions;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {

    private int transactionId;
    private String opCode;
    private List<Questions> questions;

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getOpCode() {
        return opCode;
    }

    public void setOpCode(String opCode) {
        this.opCode = opCode;
    }

    public List<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Questions> questions) {
        this.questions = questions;
    }
}
